const discord = require("discord.js");

/**
 * Przechowuje modele
 */
class ModelStore extends discord.Collection {
  /**
   * Przygotowuje przechowalnie
   * @param {Model[]} models Modele
   */
  constructor(models) {
    const arr = [];
    for (const model of models) arr.push([model.name, model]);
    super(arr);
  }
}

module.exports = { ModelStore };