import { ModelStore } from "./ModelStore";
import { Client } from "discord-rpc";

type Model = {
  name: string,
  title: string,
  images: ModelImages,
  state: string
}

type ModelImages = {
  bigKey: string,
  bigText: string,
  smallKey: string,
  smallText: string
}

type Config = {
  clientId: string,
  models: Model[]
}

private class ModelError extends Error {
  constructor(message: string, model: Model): ModelError;
  returns: ModelError
}

/**
 * Główna klasa
 */
export class Main extends Client {
  /**
   * Przygotowuje klasę
   * @param config Konfiguracja
   */
  constructor(config: Config): Main;

  /**
   * Sprawdza konfigurację
   */
  private validateConfig(): void;

  /**
   * Zmienia/Ustawia model
   * @param name Nazwa modelu
   * @param startTimestamp Czas rozpoczęcia
   * @param endTimestamp Czas zakończenia
   */
  public changeModel(name: string, startTimestamp?: Date, endTimestamp?: Date): Promise<void>

  /**
   * Ustawia datę rozpoczęcia
   * @param timestamp Data rozpoczęcia
   */
  public changeStartTimestamp(timestamp = new Date()): Promise<void>

  /**
   * Ustawia datę zakończenia
   * @param timestamp Data zakończenia
   */
  public changeEndTimestamp(timestamp = new Date()): Promise<void>

  /**
   * Wyłącza klasę
   */
  public destroy(): void
}
